---
layout: markdown_page
title: "Category Direction - Switchboard"
---

- TOC
{:toc}

## Switchboard

<!--
| | |
| --- | --- |
| Stage | [<STAGE-NAME>](/direction/<STAGE-NAME>/) |
| Maturity | [<MATURITY-LEVEL>](/direction/maturity/) |
| Content Last Reviewed | `yyyy-mm-dd` |
-->

### Introduction and how you can help

This page outlines the Direction for the GitLab Switchboard category which belongs to the [GitLab Dedicated group](https://about.gitlab.com/handbook/product/categories/#gitlab-dedicated-group). To provide feedback or ask questions about this product category, reach out to the [Product Manager](mailto:lbortins@gitlab.com).

You can contribute to this category by:

- Commenting on a relevant epic or issue in [our project](https://gitlab.com/gitlab-com/gl-infra/gitlab-dedicated/team) (GitLab internal), or opening a new issue in the [GitLab Dedicated issue tracker](https://gitlab.com/gitlab-com/gl-infra/gitlab-dedicated/team/-/issues/new?issue%5Bmilestone_id%5D=)
- Joining the discussion in the [#f_gitlab_dedicated](https://gitlab.slack.com/archives/C01S0QNSYJ2) Slack channel

To learn more about concepts and terminology discussed in this direction page, please refer to the [Switchboard Glossary](./glossary).

## Overview

### Solution

Switchboard is a portal for the [GitLab Dedicated](https://about.gitlab.com/direction/saas-platforms/dedicated/) single-tenant SaaS offering that is used both by GitLab Dedicated customers and internal GitLab teams who support and maintain GitLab Dedicated instances.

In addition to the user interface, the Switchboard application automates much of the ongoing maintenance work needed to maintain GitLab Dedicated instances, reducing the amount of time internal teams need to spend on operational tasks and freeing up bandwidth to focus on [maturing and adding features to GitLab Dedicated](https://about.gitlab.com/direction/saas-platforms/dedicated/#roadmap).

Our north star for Switchboard is to reduce time to value (TTV) for new Dedicated customers and create internal efficiencies by minimizing the assistance tenant operators need from GitLab to onboard and manage their instances.

### Why is this important

Maturing Switchboard is key to the success of GitLab Dedicated at scale that delivers the unique value of a single-tenant SaaS environment that is able to meet their needs for security and compliance without the operational burden of a self-managed environment. 

While GitLab Dedicated customers can rely on GitLab to maintain the stability and security of their environment, tenant admins can use Switchboard to quickly make configuration changes to their organization's GitLab Dedicated tenant instance. Switchboard also serves as the "front door" to GitLab Dedicated by providing a welcoming and efficient onboarding experience.

From an internal perspective, Switchboard serves as the primary connection between internal operators and GitLab Dedicated tenant instances and Switchboard enables them to operate at scale. For added security, GitLab tenant operators do not have direct access to customer tenant environments - [read more about that here](https://docs.gitlab.com/ee/subscriptions/gitlab_dedicated/#access-controls) or [review the GitLab Dedicated architecture here](https://handbook.gitlab.com/handbook/engineering/infrastructure/team/gitlab-dedicated/architecture/#high-level-overview). 

### Background

During the Beta and Limited Availability (LA) phases of GitLab Dedicated, all management of tenant instances was performed by the same internal teams who are responsible for developing the architecture and automation needed to fully scale GitLab Dedicated as a product offering. 

While some operations will continue to be handled by these teams, now that both GitLab Dedicated and Switchboard have reached general availability there are many configurations and actions that Switchboard empowers tenant operators to complete on their own, such as:
- Reviewing their organization's SAML configurations
- Configuring IP allowlists
- Adding custom certificates

Read more about how to use Switchboard in our [documentation](https://docs.gitlab.com/ee/administration/dedicated/).

### Target Audience

Switchboard shares its target audience with [GitLab Dedicated](https://about.gitlab.com/direction/saas-platforms/dedicated/#target-customer). The primary user persona for Switchboard is [Sidney (systems administrator)](https://about.gitlab.com/handbook/product/personas/#sidney-systems-administrator).

Given the short time Switchboard has been generally available, we expect to continue to refine our target audience in FY25 as we engage with a broader range of customers.

Within GitLab, Switchboard is used by the tenant operators (SREs), support, and professional services teams.

### Comparative Analysis

Similar portals have been built for other products used by Switchboard's target audience including [Managed Elasticsearch](https://www.elastic.co/elasticsearch/service), [MongoDB cloud](https://www.mongodb.com/products/tools/cloud-manager), [AWS](https://aws.amazon.com/console/features/) and [IBM power](https://www.ibm.com/products/cloud-management-console). See [Figma page](https://www.figma.com/file/YaJQT4WARRk9u0YQveioPJ/%F0%9F%AA%A6-DEPRECATED---Switchboard-onboarding?type=design&node-id=3-12118&mode=design) for more details (internal only).

We will look to common patterns shared by these portals as we continue to build out the capabilities of Switchboard.

### Success Criteria

We are still finalizing our success criteria for Switchboard. At a high level, we know that we will want to measure our impact on both customer satisfaction (e.g. decrease in Time to Value with self-serve onboarding) as well as internal efficiency (e.g. decrease time per operator spent on tenant maintenance tasks).

At a high level, maturing Switchboard is part of achieving the [SaaS Platforms mission](https://about.gitlab.com/direction/saas-platforms/#mission).

## Current state

The Switchboard application has been generally available since 2023-09-29 and is being used internally by GitLab operators to support existing GitLab Dedicated tenant instances and also by external tenant instance admins. 

Most of the internal operators' responsibilities (maintenance windows, incident response/remediation) are currently being completed with the Switchboard application and monitoring tools within the tenant instances themselves. Many formerly manual tasks have been automated or removed.

The UI currently provides the ability to onboard new tenants as well as to make changes to existing tenants using the Switchboard Configuration UI (available to tenant admins) or editing the tenant model JSON directly (available to [GitLab operators](https://about.gitlab.com/direction/saas-platforms/switchboard/glossary/#gitlab-operator) only). 

User management and audit logs for Amp jobs (internal only) are also available using the UI.

## Where are we headed

As more tenant administrators are invited to use Switchboard, GitLab internal operators will have significantly fewer interactions with individual instances and will instead focus on monitoring and maintenance across all Dedicated tenants with visibility provided by Switchboard. 

Switchboard will also enable tenant operators to monitor the health of their instances and provide them with critical insights should they need to reach out to GitLab support.

### What is next for us

The next step for Switchboard is to expand the functionality of the Configuration UI to empower tenant administrators to further configure their tenant instance and reduce the need for support requests. 

This includes adding MVCs of capabilities like customer-facing audit logs and notifications to better support tenant administrators when they make updates to their tenant configurations.

As the Switchboard onboarding flow is used by more users, we will identify improvements and add further configuration options to reduce the need for manual intervention by SREs. 

As we do this, we will need to balance how each deliverable contributes to the north star of self-service and efficiency with the potential to cause an incident that will be time-consuming for GitLab support and/or operators to remediate.

Features that allow GitLab operators to scale up their capacity for supporting tenant environments will take first priority.

Work in progress and in the backlog is being tracked on the [Switchboard Build Board](https://gitlab.com/groups/gitlab-com/gl-infra/gitlab-dedicated/-/boards/4498935?label_name%5B%5D=component%3A%3ASwitchboard), which is internal only.

### What we are currently working on

Our current focus is expanding the Configuration UI to include Reverse Private Link and Private Hosted Zone configuration along with expanding the Switchboard onboarding flow to automatically share root credentials upon tenant instance creation. 

In parallel, there is an onging workstream to [reduce operational toil](https://gitlab.com/gitlab-com/gitlab-OKRs/-/work_items/4904) (internal only) for GitLab tenant operators. In FY24 Q4, this includes our first iteration of an [internal API](https://gitlab.com/groups/gitlab-com/gl-infra/gitlab-dedicated/-/epics/266) for retrieving tenant data from Switchboard.

### What have we recently completed

The Switchboard onboarding flow was released as generally available on 2023-09-29 and the Configuration UI was released to tenant administrators on 2023-10-31.

So far in FY24 Q4, the Switchboard team has also released significant improvements to the automated maintenance process for GitLab Tenant operators - [watch a demo here](https://gitlab.com/groups/gitlab-com/gl-infra/gitlab-dedicated/-/epics/247#demo-description). (internal only)

### What is not planned right now

* Switchboard will not directly interact with customer tenant environments. Read more about that in the [GitLab Dedicated documentation](https://docs.gitlab.com/ee/subscriptions/gitlab_dedicated/#access-controls).

* Switchboard is solely focused on supporting GitLab Dedicated instances. We do not currently plan to expand it to support GitLab.com or Self-Managed instances.

## Roadmap

Below are the key capabilities we plan to add to Switchboard in the first half of FY25.

The Switchboard roadmap is closely tied to the [GitLab Dedicated roadmap](https://about.gitlab.com/direction/saas-platforms/dedicated/#roadmap) (we often have dependencies on the Environment Automation team or vice versa), however it is tracked separately as most Switchboard work can now be completed independently.

We are continuing to gather internal feedback and feature requests via this [feedback issue](https://gitlab.com/gitlab-com/gl-infra/gitlab-dedicated/team/-/issues/2078) (internal).

### FY25 Q1

- [MVC: customer-facing audit logs](https://gitlab.com/groups/gitlab-com/gl-infra/gitlab-dedicated/-/epics/337)
- [MVC: email notifications](https://gitlab.com/groups/gitlab-com/gl-infra/gitlab-dedicated/-/epics/338)
- [Fully automate creation of Geo-enabled Dedicated Tenant Instances](https://gitlab.com/gitlab-com/gl-infra/gitlab-dedicated/team/-/issues/3688)
- [Add import of geo-based migration secrets to Switchboard flows](https://gitlab.com/groups/gitlab-com/gl-infra/gitlab-dedicated/-/epics/328)

### FY25 Q2

- [Release Reverse Private Link Config UI](https://gitlab.com/groups/gitlab-com/gl-infra/gitlab-dedicated/-/epics/284)
- [Release Private Hosted Zones Config UI](https://gitlab.com/groups/gitlab-com/gl-infra/gitlab-dedicated/-/epics/275)
- [MVC: tenant status dashboards (internal users only)](https://gitlab.com/groups/gitlab-com/gl-infra/gitlab-dedicated/-/epics/153)
- POC: Switchboard for Hosted Runners
- Configurations UI Pajamas migration

  For additional initiatives we are considering for FY25 Q2, see our [Q2 planning issue](https://gitlab.com/gitlab-com/gl-infra/gitlab-dedicated/team/-/issues/3841)

### FY25 Q3

- MVC: Switchboard for Hosted Runners
- Tenant Status Dashboards (external users)

### FY25 Q4

- Iterate on onboarding flow design
- User management

### FY26

- Integration with CustomersDot
- MVC: external API for tenant updates


