---
layout: markdown_page
title: "Group Direction - Product Analytics"
description: "The Product Direction for GitLab's Product Analytics Group."
canonical_path: "/direction/monitor/product-analytics/"
---

- TOC
{:toc}

Welcome to the GitLab Product Analytics group direction. 

# Overview and Vision
The Product Analytics group vision is in alignment and contributes to the [Monitor Stage vision](/direction/monitor/#vision) and simply stated is ". .  to continue to extend DevOps across its most painful gap - measuring user value." Giving teams the tools they need to stay user-focused can have positive impact on their performance, job satisfaction and productivity as noted in the [Accelerate State of DevOps 2023](https://cloud.google.com/blog/products/devops-sre/announcing-the-2023-state-of-devops-report).

## What is Product Analytics?
GitLab's Product Analytics feature empowers businesses building applications to make data-driven decisions, optimize user experiences, drive growth, and stay competitive by focusing on customer value. Product Analytics provides key information about who your users are, how they behave, their common adoption patterns and workflows, and any friction points in key business critical funnels through your application. By keeping this information close to your development teams, they can quickly see how their code translates to user outcomes.

## How can you use Product Analytics data in your DevSecOps lifecycle?
* **Understand user behavior**: Product analytics provides insights into how users interact with an application. It tracks user actions, such as clicks, navigation paths, and feature usage, helping businesses understand user behavior patterns. This understanding is invaluable for optimizing user experience, identifying pain points, and making data-driven decisions to enhance the product.
* **Identify friction points in the UX**: By analyzing user behavior, businesses can identify areas where users may encounter difficulties or frustrations within the application. With this information, UX designers and developers can make informed changes to improve the user interface, streamline processes, and enhance overall usability.
* **Feature Prioritization**: By helping GitLab customers focus on the features and workflows that deliver the most value to their users, businesses can allocate resources effectively and drive product innovation.
* **Measuring Product Performance**: Product analytics provides quantitative data on key performance indicators (KPIs) such as user engagement, retention rates, conversion rates, and churn rates. By monitoring these metrics, businesses can evaluate the success of their application and track progress over time.
* **Optimizing Conversion Funnel**: For applications with conversion goals (such as sign-ups, purchases, or subscriptions), product analytics helps optimize the conversion funnel. By analyzing user behavior at each stage of the funnel, businesses can identify barriers to conversion and implement targeted strategies to improve conversion rates.
* **Iterative Improvement**: Product analytics supports an iterative approach to product development by providing continuous feedback on user interactions and outcomes. Businesses can use this feedback loop to test hypotheses, iterate on features, and validate product enhancements, leading to a more responsive and customer-centric development process.


## Our unique opportunity
Combining Product Analytics with the comprehensive suite of tools offered by GitLab’s One Platform yields a multitude of benefits for teams and organizations. By leveraging Product Analytics alongside GitLab's robust version control, issue tracking, CI/CD pipelines, and collaboration features, teams gain unprecedented visibility into the entire secure software development lifecycle. They can seamlessly track user behavior, gather valuable adoption insights, and make data-driven decisions right from within their development environment. This integration empowers teams to iterate rapidly, prioritize features effectively, and optimize user experiences based on real-time feedback. Additionally, by centralizing data within GitLab, teams can streamline communication, foster collaboration, and ensure alignment across departments, ultimately driving greater efficiency and innovation throughout the development process.

Product managers can track an issue all the way from identification to release, and now go a step further to validate that the bug resolution or new feature has the expected customer impact, all without leaving the GitLab platform.  The same goes for the development team, who now no longer need access to a separate tool to ensure that the code they are writing has the desired effect on the user outcomes. 

With our GA release of Product Analytics, we also aim to simplify the instrumentation process that is a common pain point for customers.  We are collaborating with the [Analytics Instrumentation Group](https://about.gitlab.com/direction/monitor/analytics-instrumentation) who are providing interfaces to developers to enable the gathering of insights. And, by storing dashboard configuration-as-code (CaC), we can follow developer best practices for iterative development and version control of their custom visualizations.
In addition to the benefits mentioned previously, our ability to store the user's application code presents a significant longer-term opportunity. By leveraging this contextual information, we can offer more comprehensive analytics solutions than any other product. For instance, we could explore the possibility of suggesting or automatically adding analytics instrumentation code to different parts of the app. While this requires extensive research, one potential outcome could be direct suggestions within merge requests (MRs) along with code snippets to automatically instrument newly added sections of code.

Finally, GitLab's unique capabilities with respect to AI provide opportunities that will be difficult for other companies to replicate. GitLab Duo and other AI features will give us the ability to help address pain points we've identified as well as helping improve user workflows. Some ideas around this relate to offering automatic code for instrumentation, automated suggestions for reporting and charting of collected data, and providing a question-and-answer interface for engaging with the data. Since AI is so new, we are actively exploring this area and tracking it in this [epic](https://gitlab.com/groups/gitlab-org/-/epics/11932).


## Use Cases
There are many use cases for Product Analytics. One way you can think about these is to segment them by the type of digital product analyzes and the subsequent questions those who create it would seek to be answer. 

### Starting Criteria
Our initial use case is focused on our ability to [dogfood](/handbook/values/#dogfooding) GitLab’s own applications. Our initial starting criteria for a customer profile is that they:
* Develop applications, this can be internal or external facing
* Have a particular interest in views, customer behavior partners, usage funnels, and conversion pain points

### Our ideal first customer
The ideal customers for Product Analytics are customers already invested in the GitLab Ultimate DevSecOps platform. Customers may be exploring GitLab's Product Analytics offering for various reasons. They may be on the journey of tool consolidation and see the vision of how the GitLab One Platform can streamline their workflows. Other customers may already have invested in a solution like MixPanel or Amplitude, but feel like it is a siloed tool (meaning that only a PM or product leadership reviews the data) and are looking to reevaluate the way they integrate this data into their day to day analysis. Finally, they may have no product analytics tracking today, leaving them with a blindspot around customer behavior and adoption trends that are critical to building a data-driven roadmap for their applications. Any of these use cases make them an ideal candidate for evaluating Product Analytics.

#### Use Cases under exploration
Understanding the relationship between how your system operates and how your customers perceive the value of that system is critical to maintaining an optimized environment. Because GitLab is uniquely positioned to collect, store, and visualize data from multiple sources, we can provide an end-to-end solution for monitoring that complete story.

* Marketing analytics for marketing and sales organizations
    * This might include capabilities such as page clicks or traffic trends over time and how they relate to the effectiveness of a marketing campaign or lead generation strategy.
    * Example companies in this space: [Google Analytics](https://analytics.google.com/analytics/web)
* Application performance monitoring for software reliability engineering organizations
    * This includes capabilities such as crash debugging, error tracking analytics, meshed with product health measures, uptime measurements, and reliability metrics.
    * Example companies in this space: [Sentry](https://sentry.io/welcome/), [Firebase Crashlytics](https://firebase.google.com/products/crashlytics), [Raygun](https://raygun.com/), and [Rollbar](https://rollbar.com/)
    * The [Observability Group](/direction/monitor/observability) will build core experiences to address the use cases that we can, then consolidate into robust dashboard visualizations that include Product Analytics data as well.
* GitLab Usage Analytics
    * This includes data like DORA metrics or Value Stream Analytics, which are use cases for [Value Stream management](/direction/value_stream_management).
* Drop-in replacements for 3rd party APIs
    * Our instrumentation APIs should be easy to use and built on open-source tools, such as [Snowplow](https://github.com/snowplow), but we are not targeting explicit API compatibility with other offerings.

Note that while the above use cases and personas were not the initial focus of Product Analytics, we see the unique opportunity GitLab platform has to provide a unified visualization experience that combines system health and customer value.

# Strategy and Product Categories
Our Strategy to achieve this vision is to start by helping users store, query and visualize quantitative data to measure user value. We will collaborate with the Analytics Instrumentation team to give users the tools they need to instrument and collect data from deployed applications.

## Current Categories
Given our focus on developers, the software delivery value stream, and DevOps - we will compose our new DevOps stage, Analyze, based on the set of categories we commonly see in User Engagement [competitors](#competitors).

Currently the Product Analytics group includes two categories.

- Product Analytics Visualizations - This category is focused on helping customers understand and gain insights from usage patterns in their software.
- Product Analytics Data Management - This category is focused on the Bring Your Own Cluster capabilities to support the Self Managed customers implementing Product Analytics.

In the future we envision more categories as we broaden our scope to cover additional personas and user cases. Those new categories include:
- [Experiments](https://gitlab.com/groups/gitlab-org/-/epics/8445) - Crafting hypothesis driven tests to determine product improvement success
- In-App Messaging - Engage users from directly within your product and shifting to qualitative data collection
- Customer Surveys - Conduct polls and other inquiries to determine customer preference, reaction and satisfaction continuing down the path of qualitative data
- Session Recording - Understanding and analyzing detailed individual usage patterns

### Additional Categories of the outer loop 
There are a number of existing (or considered) product categories in GitLab that could be considered part of the outer loop that the Analytics section will partner closely with to ensure we provide a cohesive experience. Those include:
- [Error Tracking](/direction/monitor/observability/error_tracking/) - For user experienced errors
- [Feature Flags](/direction/delivery/feature_flags/) - For experiments and A/B testing
- [Service Desk](/direction/service_management/service_desk/) - For Customer Surveys and feedback
- Real User Monitoring - For Error Tracking and Session Recording

We plan to collaborate and build with these categories where possible, rather than re-inventing new solutions for these related use cases.


### What we are currently working on

#### Expanding our data query and visualization experiences
The Product Analytics team is now focused on expanding the data visualization capabilities to handle more complex, [cross-stage use cases](https://gitlab.com/groups/gitlab-org/-/epics/13798) that include Observability and Value Stream data sources. We are working to add [table stakes dashboard features](https://gitlab.com/groups/gitlab-org/-/epics/13801) that empower our customers to query data from multiple sources in a unified manner, thus unlocking the ability to tell an end-to-end, system health-to-customer value story in one space.

Additionally, the team is evaluating our deployment options, architecture, and infrastructure options to ensure that customers have a seamless experience when onboarding both Observability and Product Analytics.

Some of these features to improve the overall experience are:
* Improved data querying using the [filter bar in the Visualization designer](https://gitlab.com/groups/gitlab-org/-/epics/13685)
* First MVC for [funnel analysis](https://gitlab.com/groups/gitlab-org/-/epics/9016)
* Ability to [duplicate a dashboard](https://gitlab.com/gitlab-org/gitlab/-/issues/461079) to unlock the ability for GitLab to deliver pre-built dashboards that users can use as a starting point for their own custom dashboards
* Support for a [markdown tile visualization](https://gitlab.com/gitlab-org/gitlab/-/issues/465840) on dashboards 
* Experiments at enriching results with data from other GitLab features, like release annotations or feature flags
* Improved overall performance 
* Investigation of [Cloud Connector](https://gitlab.com/gitlab-org/gitlab/-/issues/467662) as a deployment option to align with Observability

We will respond to feedback to ensure we maintain a focus on building a "Lovable" product.

### What we recently completed

#### General availability of Product Analytics
With release [16.11](https://about.gitlab.com/releases/2024/04/18/gitlab-16-11-released/#understand-your-users-better-with-product-analytics*), Product Analytics capabilities became generally available for all users to take advantage of, just like any other capability within GitLab Ultimate.
Our minimal feature set for GA includes:
* Adding schema-driven dashboards as a way to visualize and consume information from Analytics. Our first iterations delivered two prebuilt, hardcoded, dashboards to evaluate common audience and behavior patterns in your data.
* Custom visualization designer that allows users to query their Product Analytics data and create visualization configurations that are stored as code. 
* Multiple visualization options for dashboards to present data for users in the way most valuable to them.
* Enabling users to bring their own Product Analytics cluster.

Our first iterations were focused on an internal preview of Product Analytics that we can [dogfood](https://about.gitlab.com/handbook/product/product-processes/#dogfood-everything), and we will continue to pursue this model throughout the company. This let us work through the technical questions of how to best develop Product Analytics, how to host and maintain relevant infrastructure, as well as how to use it like an end-user would. This culminated with us adding Product Analytics to the internal handbook, Pajamas, Metrics Dictionary, docs, and VersionApp for dogfooding purposes.
You can also see our [end-to-end walkthrough video](https://www.youtube.com/watch?v=Sab59A0X_ck&list=PL05JrBw4t0Kqfb4oLOFKkXxNrBJzDQ3sL&index=3) or check out the [16.11 LinkedIn Live event](https://www.linkedin.com/events/7191139444916146176/comments/) for a demo!


## Competitor Pricing
Due to the heavy emphasis on SaaS and the high data volumes - most pricing in this market is consumption-based. 
- [MixPanel](https://mixpanel.com/pricing/) - SaaS - Pay by the Monthly Tracked User
- [Heap](https://heap.io/pricing) - SaaS - Single Fee sessions/year limits
- [Pendo](https://www.pendo.io/pricing/) - SaaS - Unlimited Seats - tiered by sophistication of capabilities (Roadmapping, feedback in higher tiers)
- [LogRocket](https://logrocket.com/pricing) - SaaS - Seats + Session Limits for lower tiers - features in higher tiers.
- [PostHog](https://posthog.com/pricing) - Self-Managed - By Event Volume

# How we do it

## Performance Indicators
Our Performance Indicator is Weekly Active Users defined as users viewing a dashboard and is being tracked in [Sisense](https://app.periscopedata.com/app/gitlab/1140634/Analyze-Product-Analytics?widget=16722272) (internal link). 

## Jobs to Be Done
We are [conducting research on critical jobs to be done](https://gitlab.com/gitlab-com/Product/-/issues/4274) for the Analytics section.

## Personas
The existing personas we serve are below, in priority order. We will likely [need additional personas](https://gitlab.com/gitlab-com/Product/-/issues/4273) in the future.

1. [Sasha - Software Developer](/handbook/product/personas/#sasha-software-developer)
1. [Parker - Product Manager](/handbook/product/personas/#parker-product-manager)

Some nuance can be added to our personas and how we approach them. Nearly all analytics questions, workflows, funnels, or any metrics gathering will require technical work to add instrumentation, test, and deploy it. This is the reason we are focusing on Sasha as our primary persona before Parker. We are addressing Sasha in the context that they are supporting Analytics efforts for their team. This means they are interested in how to do tasks related to adding instrumentation code, deploying it, and debugging it in support of analytics-related questions and projects. This is a more focused version of the Sasha overall persona.

As part of considering these personas, consider what personas we are _not_ including in this initial list. Specifically, we are not targeting executive personas or Directors with the initial offering. Sasha and Parker are individual contributors and have unique needs different than Directors or executives. They are focused mainly on specific applications and the analytics related to them, whereas executives and Directors will be concerned about multiple, or a "fleet", of applications. We intend to go after these personas eventually and will not intentionally create capabilities that exclude them, but they are not our primary focus at this point.

# Competitors
The market is divided between big tech entrants building on top of complete Marketing Automation platforms marketed towards enterprise marketing orgs and stand-alone tools user engagement tools that are marketed towards Product (and occasionally Development) teams.

* **Enterprise Marketing Automation** - Some examples of these are Adobe’s [RT CDP](https://business.adobe.com/products/real-time-customer-data-platform/RTCDP.html), Oracle’s [Unity](https://www.oracle.com/cx/customer-data-platform/), Twilio’s [Segment](https://segment.com/) and Microsofts [Dynamic 365 CDP](https://dynamics.microsoft.com/en-us/customer-data-platform/). This class of products all focus on customer data collection for the purposes of marketing automation with limited direct customer engagement tools. The primary buyer for this class of tools is a CMO. Interestingly, **many of these tools focus their attention on developer-enabled data management** as that is a critical stepping stone to enabling Marketing and Product teams ([mParticle](https://www.mparticle.com/?)). 
* **User Engagement** - Another class of products is targeting smaller organizations and Product and Development teams directly. These include [Pendo](https://www.pendo.io/), [Amplitude](https://amplitude.com/), [PostHog](https://posthog.com/), [LogRocket](https://logrocket.com/), [GlassBox](https://www.glassbox.com/?utm_source=ppc&utm_medium=google&utm_campaign=brand&lsd=OA-PPC-Adwords&gclid=CjwKCAiA9aKQBhBREiwAyGP5lSLyGJZUQIIli9pacrvj18QYa4SXm2XPSZnI9twyNEjD-WulIgN1qBoC1a4QAvD_BwE) and Google’s [Firebase Engage](https://firebase.google.com/products-engage). The primary buyer of this class of tools is a CTO/CPO. Interestingly, **few of these products offer both a SaaS and Self-Managed option**.
