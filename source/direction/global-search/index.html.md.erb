---
layout: markdown_page
title: "Product Direction - Global Search"
description: "GitLab supports Advanced Search for GitLab.com and Self-Managed instances. This provides users with a faster and more complete search."
canonical_path: "/direction/global-search/"
---

- TOC
{:toc}

# Global Search

| Category | Description |
| ------ |  ------ |
| [Code Search](/direction/global-search/code-search/) | Search through all your code |
| Code Navigation | Codes language intelligence. Find all references, Jump to Definition, explore all repositories |

Global Search is managed by the [Global Search Group](https://about.gitlab.com/handbook/product/categories/#global-search-group) of the [Data Stores stage](https://about.gitlab.com/direction/core_platform/#data-stores) as part of the [Core Platform Section](https://about.gitlab.com/direction/core_platform/). 

We encourage you to share feedback via [scheduling a call](https://calendly.com/bvenker-gitlab/30m-meeting-ben-venker).

You can view our current and future work at the below links:
- Our high-level [roadmap](https://gitlab.com/groups/gitlab-org/-/epic_boards/1056673?label_name%5B%5D=group::global%20search&label_name%5B%5D=Roadmap) shows what we're working on now, next, and later
- Our [Epics by workstream board](https://gitlab.com/groups/gitlab-org/-/epic_boards/1056733?label_name%5B%5D=Roadmap&label_name%5B%5D=group::global%20search) shows the work we're doing or have planned in each of our active workstreams
- You can also view our [Issues by workstream board](https://gitlab.com/gitlab-org/gitlab/-/boards/7434592?label_name%5B%5D=group::global%20search) if you want a more granular view
- Finally, you can always see what is actively being worked on by looking at our [current milestone board](https://gitlab.com/groups/gitlab-org/-/boards/4440461?milestone_title=Started&label_name%5B%5D=group::global%20search).

## Overview

Finding anything in GitLab should be straightforward.

Global Search is the core search feature for GitLab, as the one place to search across all projects, groups, and scopes and serves to unify GitLab.

There are two modes Global Search can operate in, depending on the instance configuration:
- [Basic search](https://docs.gitlab.com/ee/user/search/#basic-search) is the default search mode for GitLab, requiring no additional configuration but providing a limited feature set.
- [Advanced search](https://docs.gitlab.com/ee/user/search/advanced_search.html) provides a richer search experience and is available at GitLab Premium self-managed, GitLab Premium SaaS, and higher tiers. It provides access to additional search scopes, advanced filters, and cross-project code search. In self-managed instances, it requires integration with [Elasticsearch](https://docs.gitlab.com/ee/integration/elasticsearch.html) or [OpenSearch](https://opensearch.org/docs/latest/).

## Vision

To create a transformative search experience within GitLab that intelligently connects users to the information they need, fostering productivity and innovation at every level of the platform.

## Strategy

To build a high-performance, AI-powered search platform that enhances user experience, anticipates the needs of our feature teams, and operates at scale, providing a stable foundation for innovation across GitLab.


### AI Search

- Provide [hybrid search capabilities](https://gitlab.com/groups/gitlab-org/-/epics/11748) across GitLab to combine traditional and AI-powered search methods, such as semantic search.
- Provide and support a vector store for [vector embeddings](https://docs.gitlab.com/ee/development/ai_features/embeddings.html), including interfaces for generating, storing, retrieving, and updating them.

### Advanced Search Framework

- [Simplify Advanced Search integration for all feature teams](https://gitlab.com/groups/gitlab-org/-/epics/13873), promoting robust search feature development.
- Enable easy [indexing and searching of customer-provided data](https://gitlab.com/gitlab-org/gitlab/-/issues/452038) to support diverse user and customer needs. 

### Optimized Performance

- [Migrate inefficient searches to Elasticsearch](https://gitlab.com/groups/gitlab-org/-/epics/13112) to enhance speed and accuracy.
- Maintain high performance and reliability standards, ensuring fast and accurate results.

### Search UI and UX

- Continuously [improve the command palette](https://gitlab.com/groups/gitlab-org/-/epics/12031) for intuitive search and action.
- Work with feature teams to [improve search UX consistency across the product](https://gitlab.com/gitlab-org/ux-research/-/issues/2656).

### Search as a Platform

- Promote search as a foundational platform, evangelizing our simple integrations and enabling innovation across teams.
- Ensure the platform is flexible and scalable, adapting to evolving user requirements.
