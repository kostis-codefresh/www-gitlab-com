---
layout: markdown_page
title: "Category Direction - Provision - Add-on Provisioning"
description: "Strategy page for the category of add-on provisioning owned by group Provision."
---
 
## On this page
{:.no_toc .hidden-md .hidden-lg}
 
- TOC
{:toc}

<link rel="stylesheet" type="text/css" href="/stylesheets/biztech.css" />
 
## Mission
The Provision group's mission is to provide a seamless customer experience in accessing GitLab subscriptions, add-ons and trials, while providing key license delivery and usage data to internal teams for data-driven insights.

## Overview
The category of `Add-on Provisioning` focuses on providing access to GitLab's supplemental products that can be purchased alongside a main subscription plan. For these products, we provide purchase confirmation mailers, paid and trial feature activation/de-activation, and user-based provisioning.

Some examples of add-on products GitLab sells include Compute Minutes, Storage, Enterprise Agile Planning and Duo Pro.

## Team Focus Areas
### Top Priorities FY25
Over the next 12 months, the Provision team has three primary objectives:

1. Support the release of new product offerings
1. Expand trial availability and experience
1. Improve the user seat assignment experience
1. Streamline the license generation process

#### Support release of new add-on product offerings
One of Provision's primary focus areas this year will be supporting the release of new products and features. We assist with ensuring features are accurately provisioned to customers on GitLab.com, GitLab Dedicated, and self managed instances. This year, we plan to support the release of the Duo Enterprise add-on. In FY24, we helped to launch:
 - [GitLab Duo Pro](https://about.gitlab.com/gitlab-duo/) for [GitLab.com](https://gitlab.com/groups/gitlab-org/-/epics/10336), [GitLab Dedicated](https://gitlab.com/groups/gitlab-org/-/epics/11715), and [self managed instances](https://gitlab.com/groups/gitlab-org/-/epics/10613) as well as [Enterprise Agile Planning](https://gitlab.com/groups/gitlab-com/packaging-and-pricing/ghidorah/-/epics/5).

#### Expand and improve trial availability
As GitLab continues to expand it's feature portfolio, we recognize the importance of allowing customers to trial these features ahead of committing to a purchase. In FY25, we intend to introduce a trial for [GitLab Duo Pro](https://gitlab.com/groups/gitlab-org/-/epics/10988) and [GitLab Duo Enterprise](https://gitlab.com/groups/gitlab-org/-/epics/13775) for all deployment types.

#### Improve the add-on seat assignment experience
[GitLab Duo Pro](https://about.gitlab.com/gitlab-duo/) and [Enterprise Agile Planning](https://about.gitlab.com/pricing/) are the first GitLab product offerings requiring user-based assignment. In FY24, we introduced user provisioning for GitLab Duo Pro. This year, we plan to continue to iterate on this experience by introducing [user sorting and filtering](https://gitlab.com/groups/gitlab-org/-/epics/11276) and [the ability to assign users in bulk](https://gitlab.com/groups/gitlab-org/-/epics/11462). In addition, we intend to begin supporting user-based provisioning and enforcement for Enterprise Agile Planning. 

### Other Attention Areas
Besides the top initiatives outlined in our 1 year plan, we have some additional areas of attention outlined below. For a comprehensive list of our upcoming and ongoing projects, check out our [GitLab Epic Roadmap](https://gitlab.com/groups/gitlab-org/-/roadmap?state=all&sort=start_date_asc&layout=QUARTERS&timeframe_range_type=THREE_YEARS&label_name%5B%5D=Fulfillment+Roadmap&label_name%5B%5D=group%3A%3Aprovision&progress=COUNT&show_progress=true&show_milestones=false&milestones_type=GROUP).

