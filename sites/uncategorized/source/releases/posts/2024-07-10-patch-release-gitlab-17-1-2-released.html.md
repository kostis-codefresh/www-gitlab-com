---
title: "GitLab Critical Patch Release: 17.1.2, 17.0.4, 16.11.6"
categories: releases
author: Rohit Shambhuni
author_gitlab: rshambhuni
author_twitter: gitlab
description: "Learn more about GitLab Critical Patch Release: 17.1.2, 17.0.4, 16.11.6 for GitLab Community Edition (CE) and Enterprise Edition (EE)."
canonical_path: '/releases/2024/07/10/patch-release-gitlab-17-1-2-released.html.md'
image_title: '/images/blogimages/security-cover-new.png'
tags: security
---

<!-- For detailed instructions on how to complete this, please see https://gitlab.com/gitlab-org/release/docs/-/blob/master/general/patch/blog-post.md -->

Today we are releasing versions 17.1.2, 17.0.4, 16.11.6 for GitLab Community Edition (CE) and Enterprise Edition (EE).

These versions contain important bug and security fixes, and we strongly recommend that all GitLab installations be upgraded to
one of these versions immediately. GitLab.com and GitLab Dedicated are already running the patched version.

GitLab releases fixes for vulnerabilities in dedicated patch releases. There are two types of patch releases:
scheduled releases, and ad-hoc critical patches for high-severity vulnerabilities. Scheduled releases are released twice a month on the second and fourth Wednesdays.
For more information, you can visit our [releases handbook](https://handbook.gitlab.com/handbook/engineering/releases/) and [security FAQ](https://about.gitlab.com/security/faq/).
You can see all of GitLab release blog posts [here](/releases/categories/releases/).

For security fixes, the issues detailing each vulnerability are made public on our
[issue tracker](https://gitlab.com/gitlab-org/gitlab/-/issues/?sort=created_date&state=closed&label_name%5B%5D=bug%3A%3Avulnerability&confidential=no&first_page_size=100)
30 days after the release in which they were patched.

We are dedicated to ensuring all aspects of GitLab that are exposed to customers or that host customer data are held to
the highest security standards. As part of maintaining good security hygiene, it is highly recommended that all customers
upgrade to the latest patch release for their supported version. You can read more
[best practices in securing your GitLab instance](/blog/2020/05/20/gitlab-instance-security-best-practices/) in our blog post.

### Recommended Action

We **strongly recommend** that all installations running a version affected by the issues described below are **upgraded to the latest version as soon as possible**.

When no specific deployment type (omnibus, source code, helm chart, etc.) of a product is mentioned, this means all types are affected.

## Security fixes

### Table of security fixes

| Title | Severity |
| ----- | -------- |
| [An attacker can run pipeline jobs as an arbitrary user](#an-attacker-can-run-pipeline-jobs-as-an-arbitrary-user) | Critical |
| [Developer user with `admin_compliance_framework` permission can change group URL](#developer-user-with-admin_compliance_framework-permission-can-change-group-url) | Medium |
| [Admin push rules custom role allows creation of project level deploy token](#admin-push-rules-custom-role-allows-creation-of-project-level-deploy-token) | Low |
| [Package registry vulnerable to manifest confusion](#package-registry-vulnerable-to-manifest-confusion) | Low |
| [User with `admin_group_member` permission can ban group members](#user-with-admin_group_member-permission-can-ban-group-members) | Low |
| [Subdomain takeover in GitLab pages](#subdomain-takeover-in-gitlab-pages) | Low |

### An attacker can run pipeline jobs as an arbitrary user

An issue was discovered in GitLab CE/EE affecting versions 15.8 prior to 16.11.6, 17.0 prior to 17.0.4, and 17.1 prior to 17.1.2, which allows an attacker to trigger a pipeline as another user under certain circumstances.
This is a critical severity issue (`CVSS:3.1/AV:N/AC:L/PR:L/UI:N/S:C/C:H/I:H/A:N`, 9.6).
It is now resolved in the latest release and is assigned [CVE-2024-6385](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2024-6385).

Thanks to [yvvdwf](https://hackerone.com/yvvdwf) for reporting this vulnerability through our HackerOne bug bounty program.


### Developer user with `admin_compliance_framework` permission can change group URL

An issue was discovered in GitLab CE/EE affecting all versions starting from 17.0 prior to 17.0.4 and from 17.1 prior to 17.1.2 where a Developer user with `admin_compliance_framework` custom role may have been able to modify the URL for a group namespace.
This is a medium severity issue (`CVSS:3.1/AV:N/AC:L/PR:H/UI:N/S:U/C:N/I:H/A:N`, 4.9).
It is now mitigated in the latest release and is assigned [CVE-2024-5257](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2024-5257).

Thanks [js_noob](https://hackerone.com/js_noob) for reporting this vulnerability through our HackerOne bug bounty program.


### Admin push rules custom role allows creation of project level deploy token

An issue was discovered in GitLab CE/EE affecting all versions starting from 17.0 prior to 17.0.4 and from 17.1 prior to 17.1.2 where a Guest user with `admin_push_rules` permission may have been able to create project-level deploy tokens.
This is a low severity issue (`CVSS:3.1/AV:N/AC:L/PR:H/UI:N/S:U/C:L/I:L/A:N`, 3.8).
It is now mitigated in the latest release and is assigned [CVE-2024-5470](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2024-5470).

Thanks [indoappsec](https://hackerone.com/indoappsec) for reporting this vulnerability through our HackerOne bug bounty program.


### Package registry vulnerable to manifest confusion

An issue was discovered in GitLab CE/EE affecting all versions starting from 11.8 prior to 16.11.6, starting from 17.0 prior to 17.0.4, and starting from 17.1 prior to 17.1.2 where it was possible to upload an NPM package with conflicting package data.
This is a low severity issue (`CVSS:3.1/AV:N/AC:H/PR:L/UI:R/S:C/C:N/I:L/A:N`, 3.0).
It is now mitigated in the latest release and is assigned [CVE-2024-6595](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2024-6595).

This vulnerability was found internally by a GitLab team member [Ameya Darshan](https://gitlab.com/ameyadarshan). Thanks to [Darcy Clarke](https://x.com/darcy) for their work on [manifest confusion](https://blog.vlt.sh/blog/the-massive-hole-in-the-npm-ecosystem).


### User with `admin_group_member` permission can ban group members

An issue was discovered in GitLab CE/EE affecting all versions starting from 16.5 prior to 16.11.6, starting from 17.0 prior to 17.0.4, and starting from 17.1 prior to 17.1.2 in which a user with `admin_group_member` custom role permission could ban group members.
This is a low severity issue (`CVSS:3.1/AV:N/AC:L/PR:H/UI:N/S:U/C:N/I:L/A:N`, 2.7).
It is now mitigated in the latest release and is assigned [CVE-2024-2880](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2024-2880).

Thanks [ashish_r_padelkar](https://hackerone.com/ashish_r_padelkar) for reporting this vulnerability through our HackerOne bug bounty program.


### Subdomain takeover in GitLab Pages

An issue was discovered in GitLab CE/EE affecting all versions prior to 16.11.6, starting from 17.0 prior to 17.0.4, and starting from 17.1 prior to 17.1.2, which allows a subdomain takeover in GitLab Pages by checking if the domain is enabled every time the custom domain is resolved.
This is a low severity issue (`CVSS:3.1/AV:N/AC:L/PR:L/UI:R/S:U/C:N/I:L/A:N`, 3.5).
It is now mitigated in the latest release and is assigned [CVE-2024-5528](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2024-5528).

Thanks [fdeleite](https://hackerone.com/fdeleite) for reporting this vulnerability through our HackerOne bug bounty program.


## Bug fixes


### 17.1.2

* [git: Update `symlinkPointsToGitDir` version check](https://gitlab.com/gitlab-org/gitaly/-/merge_requests/7058)
* [Fix MailRoom not loading in Omnibus](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/157347)
* [Use static AWS credentials for elasticsearch indexer if set](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/157428)
* [ci: For 17-1 Use default Ruby version for MRs targeting stable branches](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/157655)
* [Remove transaction opening for non-basic search count](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/157430)
* [Merge branch 'echui-gitlab-master-patch-58822' into 'master'](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/157757)
* [Update FF version info for graphql_minimal_auth_methods](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/157312)
* [Merge branch 'correct_finalize_epics_backfilling' into '17-1-stable-ee'](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/157608)
* [Fix merge unverified changes modal showing incorrectly](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/158201)
* [Backport 17.1: Field needs to be called Url](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/158332)
* [Backport Release Environments notification pipeline change to 17.1](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/158452)
* [Update dependency slack-messenger to v2.3.5](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/157381)
* [Force ffi gem to use Ruby platform gem](https://gitlab.com/gitlab-org/omnibus-gitlab/-/merge_requests/7734)
* [Fix Redis password handling with reserved characters](https://gitlab.com/gitlab-org/omnibus-gitlab/-/merge_requests/7744)
* [Pin QA CI tests to stable gitlab-org/gitlab branches](https://gitlab.com/gitlab-org/omnibus-gitlab/-/merge_requests/7765)

### 17.0.4

* [Backport Release Environments notification pipeline change to 16.11](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/158455)
* [Backport Release Environments notification pipeline change to 17.0](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/158453)
* [Update dependency slack-messenger to v2.3.5](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/157383)
* [Pin QA CI tests to stable gitlab-org/gitlab branches](https://gitlab.com/gitlab-org/omnibus-gitlab/-/merge_requests/7764)
* [Fix Redis password handling with reserved characters](https://gitlab.com/gitlab-org/omnibus-gitlab/-/merge_requests/7745)

### 16.11.6

* [Update versioning info for graphql FF](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/157242)
* [Define the Ruby patch version to use in CI jobs in 16.11](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/157451)
* [For 16.11: Explicitly set Omnibus and CNG Ruby version in CI](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/158326)
* [Backport Release Environments notification pipeline change to 16.11](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/158459)
* [Update dependency slack-messenger to v2.3.5](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/157385)
* [Pin QA CI tests to stable gitlab-org/gitlab branches](https://gitlab.com/gitlab-org/omnibus-gitlab/-/merge_requests/7763)

## Updating

To update GitLab, see the [Update page](/update).
To update GitLab Runner, see the [Updating the Runner page](https://docs.gitlab.com/runner/install/linux-repository.html#updating-the-runner).

## Receive Patch Notifications

To receive patch blog notifications delivered to your inbox, visit our [contact us](https://about.gitlab.com/company/contact/) page.
To receive release notifications via RSS, subscribe to our [patch release RSS feed](https://about.gitlab.com/security-releases.xml) or our [RSS feed for all releases](https://about.gitlab.com/all-releases.xml).

## We’re combining patch and security releases

This improvement in our release process matches the industry standard and will help GitLab users get information about security and
bug fixes sooner, [read the blog post here](https://about.gitlab.com/blog/2024/03/26/were-combining-patch-and-security-releases/).
