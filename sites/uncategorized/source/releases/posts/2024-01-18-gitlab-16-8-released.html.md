---
release_number: "16.8" # version number - required
title: "GitLab 16.8 released with Google Cloud Secret Manager support and the ability to speed up your builds with the Maven dependency proxy" # short title (no longer than 62 characters) - required
author: Jocelyn Eillis # author name and surname - required
author_gitlab: jocelynjane # author's gitlab.com username - required
image_title: '/images/16_8/16_8-cover-image.png' # cover image - required
description: "GitLab 16.8 released with Google Cloud Secret Manager support, the ability to speed up your builds with the Maven dependency proxy, general availability of Workspaces,  and much more!" # short description - required
twitter_image: '/images/16_8/16_8-cover-image.png' # required - copy URL from image title section above
categories: releases # required
layout: release # required
featured: yes
rebrand_cover_img: true

# APPEARANCE
# header_layout_dark: true #uncomment if the cover image is dark
# release_number_dark: true #uncomment if you want a dark release number
# release_number_image: "/images/X_Y/X_Y-release-number-image.svg" # uncomment if you want a svg image to replace the release number that normally overlays the background image

---

<!--
This is the release blog post file. Add here the introduction only.
All remaining content goes into data/release-posts/.

**Use the merge request template "Release-Post", and please set the calendar due
date for each stage (general contributions, review).**

Read through the Release Posts Handbook for more information:
https://about.gitlab.com/handbook/marketing/blog/release-posts/#introduction
-->

Today, we are excited to announce the release of GitLab 16.8 with [Google Cloud Secret Manager support](#google-cloud-secret-manager-support), [the ability to speed up your builds with the Maven dependency proxy](#speed-up-your-builds-with-the-maven-dependency-proxy), [general availability of Workspaces](#workspaces-are-now-generally-available), [
new organization-level DevOps view with DORA-based industry benchmarks](#new-organization-level-devops-view-with-dora-based-industry-benchmarks) and much more!

These are just a few highlights from the 25+ improvements in this release. Read on to check out all of the great updates below.

To the wider GitLab community, thank you for the 207 contributions you provided to GitLab 16.8!
At GitLab, [everyone can contribute](https://about.gitlab.com/community/contribute/) and we couldn't have done it without you!

To preview what's coming in next month’s release, check out our [Upcoming Releases page](/direction/kickoff/), which includes our 16.9 release kickoff video.
